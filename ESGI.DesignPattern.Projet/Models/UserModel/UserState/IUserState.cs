﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Models.UserModel.UserState
{
    public interface IUserState
    {
        void Connect(User user);
        void Disconnect(User user);

        List<Trip> GetTripsFromFriend(User user, User friend);
    }
}
