﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Models.UserModel.UserState
{
    public class DisconnectedState : IUserState
    {
        public void Connect(User user)
        {
            user.State = new ConnectedState();
        }

        public void Disconnect(User user)
        {
            throw new UserNotLoggedInException();
        }

        public List<Trip> GetTripsFromFriend(User user, User friend)
        {
            throw new UserLoggedInException();
        }
    }
}
