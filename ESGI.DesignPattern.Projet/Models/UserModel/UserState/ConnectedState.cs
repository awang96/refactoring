﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Models.UserModel.UserState
{
    public class ConnectedState : IUserState
    {
        public void Connect(User user)
        {
            throw new UserLoggedInException();
        }

        public void Disconnect(User user)
        {
            user.State = new DisconnectedState();
        }

        public List<Trip> GetTripsFromFriend(User user, User friend)
        {
            if (user.IsFriend(friend))
                return friend.Trips();
            throw new UserNotAuthorizedException();
        }
    }
}
