﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using ESGI.DesignPattern.Projet.Models.UserModel.UserState;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Models.UserModel
{
    public class UserSession
    {
        private static readonly UserSession userSession = new UserSession();

        private UserSession() { }

        public static UserSession GetInstance()
        {
            return userSession;
        }

        public bool IsUserLoggedIn(User user)
        {
            throw new DependendClassCallDuringUnitTestException(
                "UserSession.IsUserLoggedIn() should not be called in an unit test");
        }

        public User GetLoggedUser()
        {
            throw new DependendClassCallDuringUnitTestException(
                "UserSession.GetLoggedUser() should not be called in an unit test");
        }
    }

    public class User
    {
        private List<Trip> trips = new List<Trip>();
        private List<User> friends = new List<User>();
        public IUserState State { get; set; }

        public User()
        {
            State = new DisconnectedState();
        }

        public void Connect()
        {
            State.Connect(this);
        }

        public void Disconnect()
        {
            State.Disconnect(this);
        }

        public List<Trip> GetTripsFromFriend(User friend)
        {
            return State.GetTripsFromFriend(this, friend);
        }

        public bool IsFriend(User friend)
        {
            foreach (User friendFromUser in this.GetFriends())
            {
                if (friendFromUser.Equals(friend)) {
                    return true;
                }
            }
            return false;
        }

        public List<User> GetFriends()
        {
            return friends;
        }

        public void AddFriend(User user)
        {
            friends.Add(user);
        }

        public void AddTrip(Trip trip)
        {
            trips.Add(trip);
        }

        public List<Trip> Trips()
        {
            return trips;
        }
    }
}
