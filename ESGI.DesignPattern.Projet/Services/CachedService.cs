﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using ESGI.DesignPattern.Projet.Models.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Services
{
    class CachedService : IService
    {
        private IService Service { get; }
        private List<Trip> CacheTrips { get; set; } = null;
        public bool NeedReset { get; set; } = false;

        public CachedService(IService service)
        {
            this.Service = service;
        }
        public List<Trip> GetTripsByUser(User user, User friend)
        {
            if (CacheTrips == null || NeedReset)
                CacheTrips = Service.GetTripsByUser(user, friend);
            return CacheTrips;
        }
    }
}
