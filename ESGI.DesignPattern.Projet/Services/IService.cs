﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using ESGI.DesignPattern.Projet.Models.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Services
{
    interface IService
    {
        List<Trip> GetTripsByUser(User user, User friend);
    }
}
