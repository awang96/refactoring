﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using ESGI.DesignPattern.Projet.Models.UserModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESGI.DesignPattern.Projet.Services
{
    public class Service : IService
    {
        /*public List<Trip> GetTripsByUser(User user)
        {
            List<Trip> tripList = new List<Trip>();
            User loggedUser = UserSession.GetInstance().GetLoggedUser();
            bool isFriend = false;
            if (loggedUser != null)
            {
                foreach (User friend in user.GetFriends())
                {
                    if (friend.Equals(loggedUser))
                    {
                        isFriend = true;
                        break;
                    }
                }
                if (isFriend)
                {
                    tripList = DAO.FindTripsByUser(user);
                }
                return tripList;
            }
            else
            {
                throw new UserNotLoggedInException();
            }
        }*/

        public List<Trip> GetTripsByUser(User user, User friend)
        {
            return DAO.FindTripsOfFriendByUser(user, friend);
        }
    }
   
}
