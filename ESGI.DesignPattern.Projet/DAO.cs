﻿using ESGI.DesignPattern.Projet.Models.TripModel;
using ESGI.DesignPattern.Projet.Models.UserModel;
using System.Collections.Generic;

namespace ESGI.DesignPattern.Projet
{
    public class DAO
    {
        public static List<Trip> FindTripsByUser(User user)
        {
            throw new DependendClassCallDuringUnitTestException(
                        "TripDAO should not be invoked on an unit test.");
        }

        public static List<Trip> FindTripsOfFriendByUser(User user, User friend)
        {
            return user.GetTripsFromFriend(friend);
        }
    }
}
